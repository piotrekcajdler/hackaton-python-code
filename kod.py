import requests
import streamlit as st
import datetime

weather_api_key = "9989ed264cb54388968182323231506"
weather_api_url = "http://api.weatherapi.com/v1/current.json"
allergy_api_url = "https://data.twojapogoda.pl/forecasts/themed/allergies/default/1176"

st.title("Warunki pogodowe i alergeny")

location = st.text_input("Wprowadź lokalizację", "Lodz")

weather_params = {"key": weather_api_key, "q": location}
weather_response = requests.get(weather_api_url, params=weather_params)
weather_data = weather_response.json()

allergy_response = requests.get(allergy_api_url)
allergy_data = allergy_response.json()

if "error" in weather_data:
    st.error("Wystąpił błąd podczas pobierania danych pogodowych.")
else:
    current_weather = weather_data["current"]
    st.write("Temperatura:", current_weather["temp_c"], "°C")
    st.write("Wilgotność:", current_weather["humidity"], "%")
    st.write("Prędkość wiatru:", current_weather["wind_kph"], "km/h")

if "error" in allergy_data:
    st.error("Wystąpił błąd podczas pobierania danych dotyczących alergenów.")
else:
    allergens = allergy_data['forecasts']['default'][0]['allergens']
    alergens_names = ["Trawy", "Bylica", "Brzoza", "Alternaria", "Babka", "Cladosporium", "Dąb", "Komosa", "Leszczyna",
                      "Olsza", "Pokrzywa", "Szczaw", "Topola", "Wierzba"]

    data = []
    for element in allergens:
        data.append(f"{element}/5")

    st.table([alergens_names, data])

    forecasts = allergy_data['forecasts']['default']

    for forecast in forecasts:
        name = forecast['name']
        sign_desc = forecast['sign_desc']
        date_str = forecast['name']
        temp = forecast["temp"]
        temp_feel = forecast["temp_feel"]

        st.write(name)
        st.write('Pogoda:', sign_desc)
        st.write("Temperatura:", temp)
        st.write("Temperatura odczuwalna:", temp_feel)

        st.write('---')
